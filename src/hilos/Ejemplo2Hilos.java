/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hilos;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Laboratorio
 */
public class Ejemplo2Hilos {
    int x = 0;
    public Object r1;
    public Object r2;
    
    public static void main(String args[]){
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Ejemplo2Hilos().run();
            }
        });               
    }
    
    // DEAD LOCK
    
    void run(){
        this.r1 = new Object();
        this.r2 = new Object();
        
        Thread t1 = new ProcesoHilo1(this,"hilo1",20,1000);
        Thread t2 = new ProcesoHilo1(this,"hilo2",20,2000);
        Thread t3 = new ProcesoHilo2(this,"hilo3",20,1500);
          
        t1.start();
        t2.start();
        t3.start();

        for(int i = 0; i<50 ; i++){
            try {
                System.out.printf("Soy el proceso padre y tambien estoy contando %d y observando a x=%d\n",i,x );
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(ProcesoHilo1.class.getName()).log(Level.SEVERE, null, ex);
            }
        }            
    }
}

class ProcesoHilo1 extends Thread {
    String id;
    int n;
    int t;
    Ejemplo2Hilos padre;
    
    ProcesoHilo1(Ejemplo2Hilos _ejh, String _id, int _n, int _t){
        this.id = _id;
        this.n  = _n;
        this.t  = _t;
        this.padre = _ejh;
    }
    
    public void run() {
        for(int i = 0; i<n ; i++){
            try {
                synchronized(this.padre.r1){
                    this.padre.x = i;
                    Thread.sleep(this.t);
                    synchronized(this.padre.r2){
                        System.out.printf("Soy el proceso %s y estoy contando %d\n", this.id,i );
                    }
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(ProcesoHilo1.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}


class ProcesoHilo2 extends Thread {
    String id;
    int n;
    int t;
    Ejemplo2Hilos padre;
    
    ProcesoHilo2(Ejemplo2Hilos _ejh, String _id, int _n, int _t){
        this.id = _id;
        this.n  = _n;
        this.t  = _t;
        this.padre = _ejh;
    }
    
    public void run() {
        for(int i = 0; i<n ; i++){
            try {
                synchronized(this.padre.r1){
                    this.padre.x = i;
                    Thread.sleep(this.t);
                    synchronized(this.padre.r2){
                        System.out.printf("Soy el proceso %s y estoy contando %d\n", this.id,i );
                    }
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(ProcesoHilo1.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}