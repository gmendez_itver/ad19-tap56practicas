/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hilos;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Alumno
 */
public class EjemploHilos {
    int x = 10;
    static int y = 0;
    public static void main(String args[]){        
        new EjemploHilos().start();        
    }

    private void start() {
        Hilo h1 = new Hilo(this,"h1",500);
        Hilo h2 = new Hilo(this,"h2",1500);
        Hilo h3 = new Hilo(this,"h3",1500);

        h1.start();
        h2.start();
        h3.start();
        
        try {
            h2.join();
            
        } catch (InterruptedException ex) {
            Logger.getLogger(EjemploHilos.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
}

class Hilo extends Thread {
    String nombre;
    int tiempo;
    EjemploHilos padre;
    
    Hilo(EjemploHilos padre, String nombre, int tiempo){
        this.padre = padre;
        this.nombre = nombre;
        this.tiempo = tiempo;
    }
    
    @Override
    public void run(){
        this.ciclar();       
    }
    
    public void ciclar(){                        
        for(int i=0; i < this.padre.x ; i++){
           synchronized(this.padre){
                this.padre.y = i;

                try {
                    Thread.sleep(this.tiempo);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Hilo.class.getName()).log(Level.SEVERE, null, ex);
                }

                System.out.printf("%s - i = %d, y = %d\n",this.nombre,i,this.padre.y);
           }
       }                
    }
    
}
