/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hilos;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author macpro1
 */
public class ClienteSocket {
    
    public static void main(String args[]){
        String buffer = "";
        try {
            Socket conexion = new Socket("10.25.1.95",1001);            
            // Microfono
            OutputStream os =  conexion.getOutputStream();
            DataOutputStream dos = new DataOutputStream(os);

            BufferedReader reader =
                   new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Conectado!");
            
            new HiloConexion(conexion).start();
            
            while(true){                
                buffer = reader.readLine();                
                dos.writeUTF(buffer);
                
                if(buffer.equals("adios")){
                    break;
                }
            }                                             
            
            conexion.close();
            
        } catch (IOException ex) {
            Logger.getLogger(ClienteSocket.class.getName()).log(Level.SEVERE, null, ex);
        }
    } 
}

class HiloConexion extends Thread {
    Socket conexion = null;
    InputStream is = null;
    DataInputStream dis = null;
    String buffer = "";
    HiloConexion(Socket conexion){
        try {
            is = conexion.getInputStream();
            dis = new DataInputStream(is);
        } catch (IOException ex) {
            Logger.getLogger(HiloConexion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void start(){
        while(true){                
            try {
                buffer = dis.readUTF();
                System.out.printf("Server > %s\n", buffer);

                if(buffer.equals("adios")){
                    break;
                }
            } catch (IOException ex) {
                Logger.getLogger(HiloConexion.class.getName()).log(Level.SEVERE, null, ex);
            }
        }  
    }
}
