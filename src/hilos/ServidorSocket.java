/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hilos;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket; // Server el que esta atento a conexiones
import java.net.Socket;  // Cliente o la conexion cliente del "server"
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author macpro1
 */
public class ServidorSocket {

    ArrayList<HiloConexion> conexiones = null;
    
    public static void main(String args[]){
            new ServidorSocket().start();
    }
    
    void start(){
         conexiones = new ArrayList<HiloConexion>();
        try {
            ServerSocket servidor = new ServerSocket(1001); 
            while(true){
                HiloConexion hc = new HiloConexion(this,conexiones.size(), servidor.accept());
                conexiones.add(hc);
                hc.start();
            }
        } catch (IOException ex) {
            Logger.getLogger(ServidorSocket.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    void distribuye(int num, String buffer) {
        for(int i=0; i<conexiones.size(); i++){
            if(i!=num){
                try {
                    conexiones.get(i).dos.writeUTF(buffer);
                } catch (IOException ex) {
                    Logger.getLogger(ServidorSocket.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
}

class HiloConexion extends Thread {
    Socket conexion = null;
    String buffer = "";
    ServidorSocket padre;
    int num;
    InputStream is = null;
    DataInputStream dis = null;
    OutputStream os = null;
    DataOutputStream dos = null;
    
    HiloConexion(ServidorSocket _padre, int _num, Socket _conexion){
        conexion = _conexion;
        padre = _padre;
        num = _num;
        try {        
            is = conexion.getInputStream();                
            dis = new DataInputStream(is);
            os = conexion.getOutputStream();
            dos = new DataOutputStream(os);
        } catch (IOException ioe ){
            Logger.getLogger(ServidorSocket.class.getName()).log(Level.SEVERE, null, ioe);
        }
    }
    
    public void run(){
        System.out.println("Recibiendo conexion desde: "+conexion.getInetAddress().toString());
        try {        
            while(true){
                buffer = dis.readUTF();
                System.out.printf("Cliente %s > %s\n", conexion.getInetAddress().toString(),buffer);
                if(buffer.equals("adios")){
                    break;
                }
                padre.distribuye(num, buffer);
            }
            conexion.close();
        } catch (IOException ex) {
            Logger.getLogger(ServidorSocket.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ee){
            Logger.getLogger(ServidorSocket.class.getName()).log(Level.SEVERE, null, ee);
        }
    }
}